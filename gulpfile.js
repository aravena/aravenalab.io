/*  Copyright 2018 Javier Aravena
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

var gulp = require('gulp'),
    pug = require('gulp-pug'),
    livereload = require('gulp-livereload'),
    sass = require('gulp-sass')
    googleWebFonts = require('gulp-google-webfonts')
    responsive = require('gulp-responsive')
;

var sass_files = './src/sass/*.sass';
var pug_files = './src/*.pug';
var image_files = './media/**/*.{png,jpg}';
var output = './public/';
var fonts_file = './fonts.list'

gulp.task('default', ['views', 'style', 'images', 'fonts']);

gulp.task('views', function() {
    return gulp.src(pug_files)
        .pipe(pug())
        .pipe(gulp.dest(output))
        .pipe(livereload())
    ;
});

gulp.task('style', function() {
    return gulp.src(sass_files)
        .pipe(sass({
            sourcemaps: true,
            includePaths: []
        }).on('error', sass.logError))
        .pipe(gulp.dest(output + 'style/'))
        .pipe(livereload())
    ;
});

gulp.task('images', function() {
    return gulp.src(image_files)
        .pipe(responsive({
            '*.jpg': [
                {
                    width: 180,
                },
                {
                    width: 700,
                    rename: { suffix: '-700px' },
                },
                {
                    width: 320,
                    rename: { suffix: '-320px' },
                }
            ]
        }, {
            quality: 90,
            progressive: true,
            withMetadata: false
        }))
        .pipe(gulp.dest(output + 'media/'))
        .pipe(livereload())
    ;
});

gulp.task('fonts', function () {
    return gulp.src(fonts_file)
        .pipe(googleWebFonts({
            fontsDir: 'fonts',
            cssDir: 'style'
        }))
        .pipe(gulp.dest(output))
        .pipe(livereload())
    ;
});


gulp.task('watch', function() {
    livereload.listen();
    gulp.watch(pug_files, ['views']);
    gulp.watch(sass_files, ['style']);
    gulp.watch(image_files, ['media']);
    gulp.watch(fonts_file, ['fonts']);
});
